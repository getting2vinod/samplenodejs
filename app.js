var express = require('express');

// Mongoose import
var mongoose = require('mongoose');

// Mongoose connection to MongoDB (ted/ted is readonly)
mongoose.connect('mongodb://mongodbhost:27017/demousers', function (error) {
    if (error) {
        console.log(error);
    }
});

// Mongoose Schema definition
var Schema = mongoose.Schema;
var UserSchema = new Schema({
    first_name: String,
    last_name: String,
    email: String
});

// Mongoose Model definition
var User = mongoose.model('users', UserSchema);

// Bootstrap express
var app = express();

// URLS management

app.get('/', function (req, res) {
    res.send("<a href='/users'>Show Users</a>");
});

app.get('/users', function (req, res) {
    User.find({}, function (err, docs) {
        res.json(docs);
    });
});
app.get('/deleteuser/:id',function(req,res){
	if(req.params.id){
		 User.findByIdAndRemove(req.params.id, function (err, docs) {
    			if(err){
				res.send(500).status('Err',err);
				return;
			}
			res.send(200);
		});
	
	}
});
app.get('/adduser/:firstname/:lastname',function(req,res){
   if(req.params.firstname && req.params.lastname){
	console.log("in Save");
	var user = new User({"first_name":req.params.firstname,"last_name":req.params.lastname,"email":req.params.firstname + '@example.com'});		

  user.save(function(err,data){
	if(!err)
	{	console.log("Added : " + req.params.firstname); res.send(200);return;
	}
	else
	{console.log("Err:" + err);res.send(500);return;}
});
	}
});
app.get('/users/:email', function (req, res) {
    if (req.params.email) {
        User.find({ email: req.params.email }, function (err, docs) {
            res.json(docs);
        });
    }
});

// Start the server
app.listen(80);

